#!/bin/bash
set -e

if [ "$PSCI_BRANCH" == "master" ] && [ "$MAVEN_DEPLOY" == "true" ]
then
    $MAVEN_CMD deploy -Dmaven.test.skip=true
fi
