#!/bin/bash
set -e
export HELM_ARGS="--install \
                --force \
                --wait \
                --timeout 600 \
                --set harbor.secret=$HARBOR_SECRET \
                --set image.repository=$PSCI_IMAGE_PATH \
                --set-string image.tag=$PSCI_TAG \
                --set global.virtualServiceHost=$PSCI_FULL_DOMAIN \
                --set database.db_engine.$DB_TYPE.enabled=true \
                --set database.db_url="$PSCI_SUB_DOMAIN-db" \
                --namespace=$PSCI_NAMESPACE \
                $PSCI_SUB_DOMAIN \
                $CICD_PATH/helm/app"

echo $HELM_ARGS
echo "-------------------------------------------------------------------------"
helm upgrade --debug --dry-run $HELM_ARGS
echo "-------------------------------------------------------------------------"
helm upgrade $HELM_ARGS

echo $PSCI_FULL_DOMAIN > environment_url.txt
